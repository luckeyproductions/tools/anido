#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    if (argc != 6 && argc != 4) {

        printf("Usage:\n");
        printf("columns rows interval\n");
        printf(" or\n");
        printf("columns rows width height interval\n");
        return 1;
    }


    int columns     = atoi(argv[1]);
    int rows        = atoi(argv[2]);
    double width    = 1.0 / columns;
    double height   = 1.0 / rows;
    double interval = atof(argv[3]);

    if (argc == 6) {

        width  = atof(argv[3]);
        height = atof(argv[4]);
        interval = atof(argv[5]);
    }

    for (int j = 0; j < rows; ++j) {
        for (int i = 0; i < columns; ++i) {

            printf("<texanim uv=\"");
            printf("%.3f ", i      * width);
            printf("%.3f ", j      * height);
            printf("%.3f ",(i + 1) * width);
            printf("%.3f" ,(j + 1) * height);
            printf("\" time=\"%.3f\" />\n", interval * (i + columns * j));
        }
    }

  return 0;
}
