<?php header('Content-Type: text');
if (!count($_GET)) {

    echo("Usage:\n");
    echo("?c=[columns]&r=[rows]&w=[frameWidth]&h=[frameHeight]&i=[interval]\n\n");
    return;
}


if (array_key_exists("c", $_GET))
    $columns = max(1, $_GET["c"]);
else
    $columns = 1;

if (array_key_exists("r", $_GET))
    $rows = max(1, $_GET["r"]);
else
    $rows = 1;

$width  = 1.0 / $columns;
$height = 1.0 / $rows;


if (array_key_exists("w", $_GET))
    $width = max(0, $_GET["w"]);

if (array_key_exists("h", $_GET))
    $height = max(0, $_GET["h"]);

if (array_key_exists("i", $_GET))
    $interval = max(0, $_GET["i"]);
else
    $interval = 1;

for ($j = 0; $j < $rows; ++$j)
for ($i = 0; $i < $columns; ++$i) {

    $u1 = format($i * $width);
    $v1 = format($j * $height);
    $u2 = format(($i + 1) * $width);
    $v2 = format(($j + 1) * $height);
    $t = format($interval * ($i + $columns * $j));

    echo "<texanim uv=\"".$u1." ".$v1." ".$u2." ".$v2."\" time=\"".$t."\" />\n";
}

function format($x) {
    return rtrim(rtrim(number_format($x, 3, '.', ''), "0"), ".");
}
?>
