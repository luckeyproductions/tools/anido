# Anido

Spritesheet animation XML generator for Dry/Urho3D.

## Installation

```
./install.sh
```

## Usage

`anido 4 3 .23 .3 .17`
```
<texanim uv="0.000 0.000 0.230 0.300" time="0.000" />
<texanim uv="0.230 0.000 0.460 0.300" time="0.170" />
<texanim uv="0.460 0.000 0.690 0.300" time="0.340" />
<texanim uv="0.690 0.000 0.920 0.300" time="0.510" />
<texanim uv="0.000 0.300 0.230 0.600" time="0.680" />
<texanim uv="0.230 0.300 0.460 0.600" time="0.850" />
<texanim uv="0.460 0.300 0.690 0.600" time="1.020" />
<texanim uv="0.690 0.300 0.920 0.600" time="1.190" />
<texanim uv="0.000 0.600 0.230 0.900" time="1.360" />
<texanim uv="0.230 0.600 0.460 0.900" time="1.530" />
<texanim uv="0.460 0.600 0.690 0.900" time="1.700" />
<texanim uv="0.690 0.600 0.920 0.900" time="1.870" />
```

### Web

[`https://dry.luckey.games/tools/anido/?c=[columns]&r=[rows]&w=[frameWidth]&h=[frameHeight]&i=[interval]`](https://dry.luckey.games/tools/anido/)

[`https://dry.luckey.games/tools/anido/?c=1&r=4&i=.1`](https://dry.luckey.games/tools/anido/?c=1&r=4&i=.1)
```
<texanim uv="0 0 1 0.25" time="0" />
<texanim uv="0 0.25 1 0.5" time="0.1" />
<texanim uv="0 0.5 1 0.75" time="0.2" />
<texanim uv="0 0.75 1 1" time="0.3" />
```

----

`curl "https://dry.luckey.games/tools/anido/?c=5&r=3&i=1.25" > animation.xml`

```
<texanim uv="0 0 0.2 0.333" time="0" />
<texanim uv="0.2 0 0.4 0.333" time="1.25" />
<texanim uv="0.4 0 0.6 0.333" time="2.5" />
<texanim uv="0.6 0 0.8 0.333" time="3.75" />
<texanim uv="0.8 0 1 0.333" time="5" />
<texanim uv="0 0.333 0.2 0.667" time="6.25" />
<texanim uv="0.2 0.333 0.4 0.667" time="7.5" />
<texanim uv="0.4 0.333 0.6 0.667" time="8.75" />
<texanim uv="0.6 0.333 0.8 0.667" time="10" />
<texanim uv="0.8 0.333 1 0.667" time="11.25" />
<texanim uv="0 0.667 0.2 1" time="12.5" />
<texanim uv="0.2 0.667 0.4 1" time="13.75" />
<texanim uv="0.4 0.667 0.6 1" time="15" />
<texanim uv="0.6 0.667 0.8 1" time="16.25" />
<texanim uv="0.8 0.667 1 1" time="17.5" />
```
